package FreeSpace

import (
	"runtime"
	"strings"
	"syscall"

	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/goo-tools/Error"
)

// OnDir ...
func OnDir(dir string) (int64, error) {

	if strings.ToUpper(runtime.GOOS) == "WINDOWS" {
		return 0, Error.New("THIS DOES NOT WORK ON WINDOWS YET")
		/*
			h := syscall.MustLoadDLL("kernel32.dll")
			c := h.MustFindProc("GetDiskFreeSpaceExW")

			var freeBytes int64
			_, _, err := c.Call(uintptr(unsafe.Pointer(syscall.StringToUTF16Ptr(wd))), uintptr(unsafe.Pointer(&freeBytes)), nil, nil)

			if err != nil {
				return 0, err
			}

			return freeBytes, nil
		*/
	}

	var stat syscall.Statfs_t
	syscall.Statfs(dir, &stat)
	return cast.ToInt64(stat.Bavail * uint64(stat.Bsize)), nil
}
