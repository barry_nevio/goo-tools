package Tokenizer

import (
	jwt "github.com/dgrijalva/jwt-go"
)

type Claim struct {
	State struct {
		Bad     bool
		Expired bool
	}
	ID        string
	Audience  string
	ExpiresAt int64
	IssuedAt  int64
	NotBefore int64
	Issuer    string
	Subject   string
}

func Create(privateKey string, claim Claim) (string, error) {
	privateKeyParsed, err := jwt.ParseRSAPrivateKeyFromPEM([]byte(privateKey))
	if err != nil {
		return "", err
	}

	token := jwt.New(jwt.GetSigningMethod("RS256"))

	// Build the token claim
	token.Claims = &jwt.StandardClaims{
		Id:        claim.ID,
		ExpiresAt: claim.ExpiresAt,
		Audience:  claim.Audience,
		Subject:   claim.Subject,
		NotBefore: claim.NotBefore,
		IssuedAt:  claim.IssuedAt,
		Issuer:    claim.Issuer,
	}

	// Convert the token to a signed string
	tokenizedString, err := token.SignedString(privateKeyParsed)
	if err != nil {
		return "", err
	}

	return tokenizedString, nil
}

func Parse(publicKey string, tokenizedString string) (Claim, error) {

	// Instanciate the claim for a clean return
	claim := Claim{}

	// Parse public key
	publicKeyParsed, err := jwt.ParseRSAPublicKeyFromPEM([]byte(publicKey))
	if err != nil {
		return claim, err
	}

	// Parse the tokenized string
	tokenParsed, jwtParseErr := jwt.ParseWithClaims(tokenizedString, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return publicKeyParsed, nil // Verify token with public key
	})

	//  Handle parsing error
	if jwtParseErr != nil {
		// Get Validation Error
		jwtValidationErr := jwtParseErr.(*jwt.ValidationError)
		// Token was not expired, just bad
		if jwtValidationErr.Errors != jwt.ValidationErrorExpired {
			claim.State.Bad = true
			return claim, nil
		}
		// Token is expired, set the state of the claim
		claim.State.Expired = true
	}

	// Transfer the parsed token claim to the nice clean claim object for return
	thisClaim := tokenParsed.Claims.(*jwt.StandardClaims)
	claim.ID = thisClaim.Id
	claim.ExpiresAt = thisClaim.ExpiresAt
	claim.IssuedAt = thisClaim.IssuedAt
	claim.Issuer = thisClaim.Issuer
	claim.Audience = thisClaim.Audience
	claim.Subject = thisClaim.Subject
	claim.NotBefore = thisClaim.NotBefore

	return claim, nil
}
