package Error

type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}

// New ... Easily creates a new golang error
func New(text string) error {
	return &errorString{text}
}
