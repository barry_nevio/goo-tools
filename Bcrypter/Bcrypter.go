package Bcrypter

import (
	"github.com/spf13/cast"
	"golang.org/x/crypto/bcrypt"
)

func GenerateHashFromString(plainText string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(plainText), 10) // For the love of god do not set it higher than 10. It will take forever to verify.
	return cast.ToString(bytes), err
}

func CompareHashAndString(hash string, plainText string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(plainText))
	if err != nil {
		return false
	}
	return true
}
