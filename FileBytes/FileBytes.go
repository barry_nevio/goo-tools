package FileBytes

import (
	"bufio"
	"os"
)

// ReadFrom ... breaks down file to bytes and returns the byte slice
func ReadFrom(file string) (bytes []byte, err error) {

	f, err := os.Open(file)
	if err != nil {
		return
	}

	defer f.Close()

	var size int64
	var fileInfo os.FileInfo
	fileInfo, err = f.Stat()
	if err != nil {
		return
	}

	size = fileInfo.Size()

	bytes = make([]byte, size)

	// read file into bytes
	buffer := bufio.NewReader(f)
	_, err = buffer.Read(bytes)

	return bytes, nil
}

// WriteTo ...
func WriteTo(bytes []byte, file string) (err error) {

	fileHandle, err := os.OpenFile(
		file,
		os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
		0666,
	)

	if err != nil {
		return err
	}

	defer fileHandle.Close()

	_, err = fileHandle.Write(bytes)

	return nil
}
