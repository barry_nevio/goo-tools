package HTTPGet

import (
	"io/ioutil"
	"net/http"
)

// AsBytes ... Makes the http get request, returns the body as a byte slice
func AsBytes(url string) ([]byte, error) {

	// What we're going to return
	var bytes []byte
	var err error

	// Make the requet
	resp, err := http.Get(url)
	if err != nil {
		return bytes, err
	}
	defer resp.Body.Close()

	// Read the body
	bytes, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return bytes, err
	}

	return bytes, nil

}

// AsString ... Makes the http get , returns the body as a string
func AsString(url string) (string, error) {
	bytes, err := AsBytes(url)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}
