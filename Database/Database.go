package Database

import (
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/barry_nevio/goo-tools/Error"
)

type Settings struct {
	User           string `json:"user"`
	Password       string `json:"password"`
	Address        string `json:"address"`
	Port           string `json:"port"`
	DB             string `json:"db"`
	ReconnectSleep int    `json:"reconnect_sleep"`
	MaxIdleConns   int    `json:"max_idle_conns"`
	SSL            struct {
		Enabled       bool `json:"enabled"`
		SkipTLSVerify bool `json:"skip_tls_verify"`
		Cert          struct {
			CA string `json:"ca"`
		}
	}
}

type Connection struct {
	db       *sql.DB
	Settings Settings
	State    string
}

func New(settings Settings) (Connection, error) {
	conn := Connection{}
	conn.Settings = settings
	err := conn.open()
	return conn, err
}

// Open DB COnnection
func (conn *Connection) open() error {
	var err error
	var skipTLSParam string

	// SSL Enabled
	if conn.Settings.SSL.Enabled {
		// SSL for DB connection
		rootCertPool := x509.NewCertPool()
		if ok := rootCertPool.AppendCertsFromPEM([]byte(conn.Settings.SSL.Cert.CA)); !ok {
			conn.State = "ERROR"
			return Error.New("FAILED_TO_APPEND_PEM")
		}
		mysql.RegisterTLSConfig("custom", &tls.Config{
			RootCAs: rootCertPool,
		})

		// Skip TLS
		if conn.Settings.SSL.SkipTLSVerify {
			skipTLSParam = "?tls=skip-verify"
		}
	}

	conn.db, err = sql.Open("mysql", conn.Settings.User+":"+conn.Settings.Password+"@tcp("+conn.Settings.Address+":"+conn.Settings.Port+")/"+conn.Settings.DB+skipTLSParam)
	if err != nil {
		conn.State = "ERROR"
	} else {
		conn.State = "CONNECTED"
	}

	/*conn.db.SetMaxIdleConns(0)
	conn.db.SetMaxOpenConns(0)
	conn.db.SetConnMaxLifetime(1000)*/
	return err
}

// Get ... Checks the connection, tries to reconnect if there's an issue
// then returns the connection if all is good
func (conn *Connection) Get() (*sql.DB, error) {

	// Test query to see if connection is still good. See https://github.com/go-sql-driver/mysql/issues/82#issuecomment-18297683
	_, err := conn.db.Exec("DO 1")

	// Connection no good, try to reconnect
	if err != nil {
		conn.State = "STALE"
		fmt.Println("CONNECTION STALE, RECONNECT")

		//time.Sleep(time.Duration(conn.Settings.ReconnectSleep) * time.Second)

		err = conn.open()
		if err != nil {
			conn.State = "ERROR"
			return conn.db, err
		}
	}

	conn.State = "CONNECTED"

	// Connection was still good
	return conn.db, nil
}

// Close ... closes the db connection if it's open
func (conn *Connection) Close() {
	if conn.State == "CONNECTED" {
		db, err := conn.Get()
		if err == nil {
			_ = db.Close()
		}
	}
	return
}
