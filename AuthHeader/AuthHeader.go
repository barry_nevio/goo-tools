package AuthHeader

import (
	"encoding/base64"
	"net/http"
	"strings"
)

// Basic ... Where Authorization Basic header information will be stored
type Basic struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// Bearer ... Where Authorization Bearer header information will be stored
type Bearer struct {
	Token string `json:"token"`
}

// GetBasic ... Parses and returns the Authorization Basic header
func GetBasic(headers http.Header) Basic {
	b := Basic{}

	authHeader := GetAuthVal(headers)
	authHeaderSplit := strings.Split(authHeader, " ")

	// There should literally be only two items in this array
	if len(authHeaderSplit) != 2 {
		return b
	}

	// If this is not Basic, then it's the wrong Authorization Header
	if authHeaderSplit[0] != "Basic" {
		return b
	}

	// Possible this is a good header
	credsConcat, _ := base64.StdEncoding.DecodeString(authHeaderSplit[1])
	credsSplit := strings.Split(string(credsConcat), ":")

	// Set the Basic Model
	if len(credsSplit) >= 1 {
		b.Username = credsSplit[0]
	}
	if len(credsSplit) >= 2 {
		b.Password = credsSplit[1]
	}

	return b
}

// GetBearer ... Parses and returns the Authorization Bearer header
func GetBearer(headers http.Header) Bearer {
	b := Bearer{}

	authHeader := GetAuthVal(headers)
	authHeaderSplit := strings.Split(authHeader, " ")

	// There should literally be only two items in this array
	if len(authHeaderSplit) != 2 {
		return b
	}

	// If this is not Bearer, then it's the wrong Authorization Header
	if authHeaderSplit[0] != "Bearer" {
		return b
	}

	// Set the token
	b.Token = authHeaderSplit[1]

	return b
}

// GetAuthVal .. Gets the Authorization Header Value By Itself
func GetAuthVal(headers http.Header) string {
	for key, val := range headers {
		if key == "Authorization" && len(val) > 0 {
			return val[0]
		}
	}
	return ""
}
