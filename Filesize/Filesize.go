package Filesize

import "os"

// FromFile ...
func FromFile(file string) (size int64, err error) {

	var fileHandle *os.File
	var fileInfo os.FileInfo
	fileHandle, err = os.Open(file)
	if err != nil {
		return
	}

	fileInfo, err = fileHandle.Stat()
	if err != nil {
		return
	}

	size = fileInfo.Size()

	return

}
