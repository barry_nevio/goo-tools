package CompressImage

import (
	"bytes"
	"os/exec"
	"strings"

	"gitlab.com/barry_nevio/goo-tools/CommandExists"
	"gitlab.com/barry_nevio/goo-tools/Error"
	"gitlab.com/barry_nevio/goo-tools/FileBytes"
	"gitlab.com/barry_nevio/goo-tools/GetMimeType"
)

// FromFile ...
func FromFile(file string) (compressedImageBytes []byte, err error) {
	var imageBytes []byte
	imageBytes, err = FileBytes.ReadFrom(file)
	return FromBytes(imageBytes)
}

// FromBytes ...
func FromBytes(imageBytes []byte) (compressedImageBytes []byte, err error) {

	// Get mime type
	imageMimeType := GetMimeType.FromBytes(imageBytes)

	switch imageMimeType {
	case "image/png":
		return CompressPng(imageBytes)
	case "image/jpeg":
		return CompressJpeg(imageBytes)
	case "image/gif":
		return CompressGif(imageBytes)
	default:
		return compressedImageBytes, Error.New("Unsupported MimeType - " + imageMimeType)
	}

	return
}

// CompressPng ...
func CompressPng(imageBytes []byte) (compressedImageBytes []byte, err error) {

	// pngquant Command does not exist
	if !CommandExists.ByName("pngquant") {
		return compressedImageBytes, Error.New("pngquant not installed on the server")
	}

	// run pngquant
	cmd := exec.Command("pngquant", "-", "--speed", "4")
	cmd.Stdin = strings.NewReader(string(imageBytes))
	var o bytes.Buffer
	cmd.Stdout = &o
	err = cmd.Run()

	if err != nil {
		return
	}

	compressedImageBytes = o.Bytes()
	return
}

// CompressJpeg ...
func CompressJpeg(imageBytes []byte) (compressedImageBytes []byte, err error) {

	// ImageMagick Command does not exist
	if !CommandExists.ByName("convert") {
		return compressedImageBytes, Error.New("ImageMagick not installed on the server")
	}

	// JPEG compression flags with stdin/out
	imagickFlagsJpeg := []string{
		"-limit",
		"area",
		"0",
		/*"-limit",
		"memory",
		"2MB",
		"-limit",
		"map",
		"4MB",*/
		"jpg:-",
		"-sampling-factor",
		"4:2:0",
		"-strip",
		"-quality",
		"85",
		"-interlace",
		"Plane",
		"-gaussian-blur",
		"0.05",
		"-colorspace",
		"RGB",
		"jpg:-",
	}

	// run ImageMagick
	cmd := exec.Command("convert", imagickFlagsJpeg...)
	cmd.Stdin = strings.NewReader(string(imageBytes))
	var o bytes.Buffer
	cmd.Stdout = &o
	err = cmd.Run()

	if err != nil {
		return
	}

	compressedImageBytes = o.Bytes()
	return
}

// CompressGif ...
func CompressGif(imageBytes []byte) (compressedImageBytes []byte, err error) {

	// gifsicle Command does not exist
	if !CommandExists.ByName("gifsicle") {
		return compressedImageBytes, Error.New("gifsicle not installed on the server")
	}

	// GIF Compression flags with stdin/out
	gifsicleFlags := []string{
		"-O3",
		//"--colors=64",
		//"--use-col=web",
		"--lossy=100",
		"--scale",
		"0.8",
		"-",
		"-o",
		"-",
	}

	// run gifsicle
	cmd := exec.Command("gifsicle", gifsicleFlags...)
	cmd.Stdin = strings.NewReader(string(imageBytes))
	var o bytes.Buffer
	cmd.Stdout = &o
	err = cmd.Run()

	if err != nil {
		return
	}

	compressedImageBytes = o.Bytes()
	return
}
