package StringThings

import "unicode/utf8"

// FixNonUTF8 .. helps either replace a non utf8 enity with an actual utf8 one or just removes the entity if it fails
func FixNonUTF8(s string) string {
	if !utf8.ValidString(s) {
		v := make([]rune, 0, len(s))
		for i, r := range s {
			if r == utf8.RuneError {
				_, size := utf8.DecodeRuneInString(s[i:])
				if size == 1 {
					continue
				}
			}
			v = append(v, r)
		}
		s = string(v)
	}
	return s
}
